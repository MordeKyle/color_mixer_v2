﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Program: Color Mixer V2
//Author: Kyle McBride A02609917
//Date: 02/27/2014
//Description: Application that lets the user select two primary colors from two different sets of Radio buttons. 
//             The form should also have a Mix button. When the user clicks the Mix button, the form’s background 
//             should change to the color that you get when you mix the two selected primary colors.
//                                        ***SEE CHANGELOG IN SOLUTION DIRECTORY***

namespace Color_Mixer_V2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void mixButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (firstRadRed.Checked && secondRadRed.Checked) //conditions to meet color red.
                {
                    this.BackColor = Color.Red;
                }
                else if (firstRadBlue.Checked && secondRadBlue.Checked) //conditions to meet color blue.
                {
                    this.BackColor = Color.Blue;
                }
                else if (firstRadYellow.Checked && secondRadYellow.Checked) //conditions to meet color yellow.
                {
                    this.BackColor = Color.Yellow;
                }
                else if ((firstRadRed.Checked || secondRadRed.Checked) && (firstRadBlue.Checked || secondRadBlue.Checked))
                { //conditions to meet color purple.
                    this.BackColor = Color.Purple;
                }
                else if ((firstRadRed.Checked || secondRadRed.Checked) && (firstRadYellow.Checked || secondRadYellow.Checked))
                { //conditions to meet color orange.
                    this.BackColor = Color.Orange;
                }
                else if ((firstRadBlue.Checked || secondRadBlue.Checked) && (firstRadYellow.Checked || secondRadYellow.Checked))
                { //conditions to meet color green.
                    this.BackColor = Color.Green;
                }
                else
                { //display message if user did not select a color from each side.
                    MessageBox.Show("Please select a color from both sides before clicking on the Mix button.");
                }
            }
            catch (Exception ex)
            { //error message
                MessageBox.Show(ex.Message);
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        { //close the program on click of exit button.
            this.Close();
        }
    }
}
